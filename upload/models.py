import subprocess
from tempfile import NamedTemporaryFile
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.core.files import File

from main.celery import app


class Video(models.Model):
    original = models.FileField()
    mp4 = models.FileField(blank=True)
    # dash_manifest = models.FileField(blank=True)

    title = models.CharField(max_length=50)
    datetime_uploaded = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.original.name)


@receiver(post_save, sender=Video)
def convert_handler(sender, instance, **kwargs):
    if not instance.mp4:
        convert_video.delay(instance.id)


@app.task
def convert_video(video_id):
    video = Video.objects.get(id=video_id)

    temp_video = NamedTemporaryFile(suffix='.mp4', delete=False)
    temp_video.close()

    subprocess.call(['ffmpeg', '-y', '-i', video.original.path, '-strict', '-2', temp_video.name.format(video_id=video_id)])
    with open(temp_video.name, "rb") as f:
        django_file = File(f)

        video.mp4.save("{video_id}.mp4".format(video_id=video_id), django_file, save=True)

