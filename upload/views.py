from django.shortcuts import render
from rest_framework import viewsets

from upload.models import Video
from upload.serializers import VideoSerializer


class VideoViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
