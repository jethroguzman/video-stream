from django.contrib import admin

from upload.models import Video


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    pass
