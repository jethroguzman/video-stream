from rest_framework import serializers

from upload.models import Video


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = [
            'original',
            'mp4',
            'title',
            'datetime_uploaded',
        ]
